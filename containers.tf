resource "docker_container" "kong-db-tf" {
  name = "kong-database"
  image = "${docker_image.kong-db-tf.latest}"
  # networks= [ "${docker_network.kong-tf.id}", ]
  networks= [ "${module.docker-net.net-id}", ]
  env = ["${lookup(var.kong-database-user, var.kong-database)}", "${lookup(var.kong-database-db, var.kong-database)}"]
  ports {
    internal = "${lookup(var.kong-database-port, var.kong-database)}"
    external = "${lookup(var.kong-database-port, var.kong-database)}"
  }
}

resource "docker_container" "kong-migration-tf" {
  name = "kong-migration"
  image = "${docker_image.kong-tf.latest}"
  # networks= [ "${docker_network.kong-tf.id}", ]
  networks= [ "${module.docker-net.net-id}", ]
  env = [ 
    "KONG_DATABASE=${var.kong-database}",
    "${lookup(var.kong-database-host, var.kong-database)}"
  ]
  command = [ "kong", "migrations", "up" ]
  must_run = false
  depends_on = [
    "null_resource.sleep-kong-db"
   ]
}

resource "docker_container" "kong-tf" {
  name = "kong"
  image = "${docker_image.kong-tf.latest}"
  # networks= [ "${docker_network.kong-tf.id}", ]
  networks= [ "${module.docker-net.net-id}", ]
  env = [ 
    "KONG_DATABASE=${var.kong-database}", 
    "${lookup(var.kong-database-host, var.kong-database)}",
    "KONG_PROXY_ACCESS_LOG=/dev/stdout",
    "KONG_ADMIN_ACCESS_LOG=/dev/stdout",
    "KONG_PROXY_ERROR_LOG=/dev/stderr",
    "KONG_ADMIN_ERROR_LOG=/dev/stderr",
    "KONG_ADMIN_LISTEN=0.0.0.0:${var.kong-admin-http-int-port}, 0.0.0.0:${var.kong-admin-https-int-port} ssl"
  ]
  ports {
    internal="${var.kong-http-int-port}"
    external="${var.kong-http-port}"
  }
  ports {
    internal="${var.kong-https-int-port}"
    external="${var.kong-https-port}"
  }
  ports {
    internal="${var.kong-admin-http-int-port}"
    external="${var.kong-admin-http-port}"
  }
  ports {
    internal="${var.kong-admin-https-int-port}"
    external="${var.kong-admin-https-port}"
  }
  depends_on = [
    "null_resource.sleep-migration"
  ]
}

