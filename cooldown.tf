resource "null_resource" "sleep-kong-db" {
  provisioner "local-exec"{
    command = "sleep ${var.kong-db-cooldown}"
  }
  depends_on = [
    "docker_container.kong-db-tf"
  ]
}

resource "null_resource" "sleep-migration" {
  provisioner "local-exec"{
    command = "sleep ${lookup(var.migration-cooldown, var.kong-database)}"
  }
  depends_on = [
    "docker_container.kong-migration-tf"
  ]
}