resource "docker_image" "kong-db-tf" {
  name = "${lookup(var.database-image, var.kong-database)}"
}

resource "docker_image" "kong-tf" {
  name = "kong:latest"
}