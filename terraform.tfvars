kong-http-port=8000
kong-https-port=8443
kong-admin-http-port=8001
kong-admin-https-port=8444

migration-cooldown = {
    cassandra = "40"
    postgres = "20"
}