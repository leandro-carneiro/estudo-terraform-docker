variable "kong-database" {
  description = "Choose: cassandra or postgres"
}

variable "kong-database-host" {
  type = "map"
  default = {
    cassandra = "KONG_CASSANDRA_CONTACT_POINTS=kong-database"
    postgres  = "KONG_PG_HOST=kong-database"
  }
}

variable "kong-database-port" {
  type = "map"
  default = {
    cassandra = "9042"
    postgres  = "5432"
  }
}

variable "kong-database-user" {
  type = "map"
  default = {
    cassandra = "USER=null"
    postgres  = "POSTGRES_USER=kong"
  }
}

variable "kong-database-db" {
  type = "map"
  default = {
    cassandra = "DB=null"
    postgres  = "POSTGRES_DB=kong"
  }
}

variable "database-image" {
  type = "map"
  default = {
    cassandra = "cassandra:3"
    postgres  = "postgres:9.6"
  }
}

variable "kong-http-port" {}

variable "kong-https-port" {}

variable "kong-admin-http-port" {}

variable "kong-admin-https-port" {}
variable "kong-http-int-port" {
  default = "8000"
}

variable "kong-https-int-port" {
  default = "8443"
}

variable "kong-admin-http-int-port" {
  default = "8001"
}

variable "kong-admin-https-int-port" {
  default = "8444"
}

variable "kong-db-cooldown" {
  default="20"
}

variable "migration-cooldown" {
  type = "map"
}